package com.OPT.Helper.mvc;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class OPTController {

	@Autowired
	PDFHelper _pdf;

	@Autowired
	private ServletContext servletContext;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String showPage() throws IOException {

		return "WelcomePage";
	}

	@RequestMapping(value = "/InitialOptForm", method = RequestMethod.GET)
	public String showInitOpt(ModelMap model) {
		model.put("user", new User());
		return "index";
	}

	@RequestMapping("/ReplacementOPTForm")
	public String initOpt1() {
		return "ReplacementOPTForm";
	}

	@RequestMapping("/StemOPTForm")
	public String initOpt2() {
		return "StemOPTForm";
	}

	@RequestMapping(value = "downloadFile/pdf", method = RequestMethod.GET)
	public void downloadPdf(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, MalformedURLException {
		String dataDirectory = servletContext.getResource("/WEB-INF/").getPath();
		String pdfFilePath = dataDirectory + "i-765-updated.pdf";
		System.out.println("Downloading A .PDF File From The Server ....!");
		/**** Get The Absolute Path Of The File ****/
		System.out.println("Absolute Path Of The .PDF File Is?= " + pdfFilePath);
		File downloadFile = new File(pdfFilePath.toString());
		if (downloadFile.exists()) {
			Util.downloadFileProperties(req, resp, pdfFilePath.toString(), downloadFile);
		} else {
			System.out.println("Requested .PDF File Not Found At The Server ....!");
		}
	}

	@RequestMapping(value = "/finalpage", method = RequestMethod.POST)
	public String initOpt3(@Valid User user, BindingResult result) throws IOException {
		if (result.hasErrors()) {
			return "index";
		}
		List<String> list = null;
		_pdf.readFile();
		list = _pdf.getFields(list);

		String[] values = { user.getCountry2(), null, user.getMailAdd_city(), "/1", null, user.getCountry2(), "/Off",
				user.getPassport_issued_country(), null, null, null, user.getFname(), null, user.getMailAdd_type(),
				"/Off", null, user.getMailAdd_name(), null, null, null, user.getI94(), "/1", user.getMailAdd_state(),
				null, null, null, user.getMailAdd_zip(), "/Off", null, null, "/A", user.getGender(), null,
				user.getMailAdd_type(), null, user.getMname(), user.getPhone(), user.getMarital(), null,
				user.getEmail(), null, null, "/1", "/Y", null, null, user.getCurr_immigration_status(), null, null,
				user.getLname(), "/Off", null, null, null, user.getGender(), "/Off", "3", user.getMailAdd_type(), null,
				"/Y", null, "/Off", null, null, user.getMarital(), user.getMname(), null, null, "/Off", "/Y",
				user.getLname(), null, null, user.getBirth_city(), null, null, null, user.getBirth_state(), null, null,
				"/Off", null, null, null, null, null, null, null, "/N", "/Y", null, user.getMarital(), null, null, null,
				null, null, null, "/N", "b", user.getMailAdd_st(), "/A", null, user.getBirth_dob(), "/N", null, null,
				user.getPassport_exp_date(), user.getMarital(), null, "/Off", null, null, null, null, null, "/N", null,
				null, "/Off", user.getSevis(), null, null, null, null, null, "C", user.getLast_arrival_date(), null,
				user.getBirth_country(), null, "/Off", user.getPassport_number(), "/Off", "/Off", user.getFname(), null,
				user.getSSN(), user.getImmigration_status(), user.getLast_arrival_place(), "/Off", "/Off", "/Off", null,
				"/Off", "/Off", null, null, null, null, null, null, user.getMailAdd_number(), "/Off", null, "/Off",
				null, "/Off", "/Off", null, null, null, null };

		_pdf.updatePDF(list, values);
		_pdf.saveFile();
		_pdf.close();
		System.out.println("Size of values: " + values.length);
		return "finalpage";
	}
}
