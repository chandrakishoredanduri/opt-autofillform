package com.OPT.Helper.mvc;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.imageio.IIOException;
import javax.servlet.ServletContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import com.snowtide.PDF;
import com.snowtide.pdf.Document;
import com.snowtide.pdf.forms.AcroButtonField;
import com.snowtide.pdf.forms.AcroChoiceField;
import com.snowtide.pdf.forms.AcroForm;
import com.snowtide.pdf.forms.AcroTextField;

@Controller
public class PDFHelper {

	@Autowired
	private ServletContext servletContext;

	String source = "";
	String output = "";
	File pdfFile;
	AcroForm form;
	Document pdf;

	// Reading the PDF File
	void readFile() throws IIOException {
		try {
			source = "" + servletContext.getResource("/WEB-INF/i-765-1.pdf").getFile();
		} 
		catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
		System.out.println("Path: " + source);
		try {
			pdfFile = new File(source);
			pdf = PDF.open(pdfFile);
			form = (AcroForm) pdf.getFormData();
		} 
		catch (Exception e) {
			System.out.println("Internal Program Error: Unable to open file");
		}
	}

	@SuppressWarnings("rawtypes")
	List<String> getFields(List<String> map) {
		map = new ArrayList<String>();
		Collection field = form.getFieldNames();
		for (Iterator i = field.iterator(); i.hasNext();) {
			String type = i.next().toString();
			if (form.getField(type).getType() == "/Tx") {
				AcroTextField f = (AcroTextField) form.getField(type);
				map.add(f.getFullName());
			} else if (form.getField(type).getType() == "/Btn") {
				AcroButtonField f = (AcroButtonField) form.getField(type);
				map.add(f.getFullName());
			} else if (form.getField(type).getType() == "/Ch") {
				AcroChoiceField f = (AcroChoiceField) form.getField(type);
				map.add(f.getFullName());
			}
		}
		return map;
	}

	void downloadFile() throws MalformedURLException {
		String dataDirectory = servletContext.getResource("/WEB-INF/").getPath();
		String pdfFilePath = dataDirectory + "i-765-updated.pdf";
	}

	void saveFile() {
		try {
			output = servletContext.getResource("/WEB-INF/").getPath();
			System.out.println(output);
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
		try {
			form.writeUpdatedDocument(output + "/i-765-updated.pdf");
		} catch (Exception e) {
		}
	}

	void close() throws IOException {
		pdf.close();
	}

	public void updatePDF(List<String> list, String[] values) {
		System.out.println("Update Map: " + list.size());

		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i) + " : " + values[i]);
			try {
				if (values[i] != null)
					form.getField(list.get(i)).setValue(values[i]);
			} catch (Exception e) {
				System.out.println("Didn't worked! " + form.getField(list.get(i)).getType() + " . "
						+ form.getField(list.get(i)).getMappingName());
			}
		}
	}
}
