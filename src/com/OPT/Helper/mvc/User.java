package com.OPT.Helper.mvc;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;

import com.sun.istack.NotNull;

public class User {

	// Variables

	@Size(min = 5, message = "Enter atleast 5 char")
	//@Min(value = 3, message = "First name should be atleast 3 characters")
	private String fname;

	@Size(min = 5, message = "Enter atleast 5 char")
	//@Min(value = 1, message = "Last name should be atleast 1 character")
	private String lname;

	// Optional
	private String mname;

	
	//@Range(min = 7, max = 10, message = "Phont Number should not be more than 10 digits")
	private String phone;

	 @Email(message = "Email should be valid")
	//@Pattern(regexp = ".+@.+\\..+", message = "Wrong email!")
	private String email;

	private String gender;
	private String marital;

	@Size(min = 5, message = "Enter atleast 5 char")
	//@Min(value = 3, message = "Mail Address name should be atleast 3 characters")
	private String mailAdd_name;

	
	private String mailAdd_st;
	
	private String mailAdd_type;
	
	private String mailAdd_number;
	
	private String mailAdd_city;

	private String mailAdd_state;

	
	//@Pattern(regexp = "[0-9]+", message = "Wrong zip!")
	private String mailAdd_zip;
	private String physicalAddressConfirmation;

	@Size(min = 5, message = "Enter atleast 5 char")
	//@Min(value = 2, message = "Country Name should be atleast 2 characters")
	private String country1;
	private String country2;

	@Size(min = 5, message = "Enter atleast 5 char")
	//@Min(value = 3, message = "City Name should be atleast 3 characters")
	private String birth_city;
	
	private String birth_state;

	@Size(min = 5, message = "Enter atleast 5 char")
	//@Min(value = 3, message = "Your name should be atleast 3 characters")
	private String birth_country;
	
	private String birth_dob;

	
	private String SSN;
	
	private String i94;
	
	private String passport_number;
	
	private String passport_issued_country;
	private String passport_exp_date;
	
	private String sevis;
	private String last_arrival_date;
	
	private String last_arrival_place;
	
	private String immigration_status;
	
	private String curr_immigration_status;

	public User() {
		super();
	}

	public User(String fname, String lname, String mname, String phone, String email, String gender, String marital,
			String mailAdd_name, String mailAdd_st, String mailAdd_type, String mailAdd_number, String mailAdd_city,
			String mailAdd_state, String mailAdd_zip, String physicalAddressConfirmation, String country1,
			String country2, String birth_city, String birth_state, String birth_country, String birth_dob, String sSN,
			String i94, String passport_number, String passport_issued_country, String passport_exp_date, String sevis,
			String last_arrival_date, String last_arrival_place, String immigration_status,
			String curr_immigration_status) {
		super();
		this.fname = fname;
		this.lname = lname;
		this.mname = mname;
		this.phone = phone;
		this.email = email;
		this.gender = gender;
		this.marital = marital;
		this.mailAdd_name = mailAdd_name;
		this.mailAdd_st = mailAdd_st;
		this.mailAdd_type = mailAdd_type;
		this.mailAdd_number = mailAdd_number;
		this.mailAdd_city = mailAdd_city;
		this.mailAdd_state = mailAdd_state;
		this.mailAdd_zip = mailAdd_zip;
		this.physicalAddressConfirmation = physicalAddressConfirmation;
		this.country1 = country1;
		this.country2 = country2;
		this.birth_city = birth_city;
		this.birth_state = birth_state;
		this.birth_country = birth_country;
		this.birth_dob = birth_dob;
		SSN = sSN;
		this.i94 = i94;
		this.passport_number = passport_number;
		this.passport_issued_country = passport_issued_country;
		this.passport_exp_date = passport_exp_date;
		this.sevis = sevis;
		this.last_arrival_date = last_arrival_date;
		this.last_arrival_place = last_arrival_place;
		this.immigration_status = immigration_status;
		this.curr_immigration_status = curr_immigration_status;
	}

	/**
	 * @return the sSN
	 */
	public String getSSN() {
		return SSN;
	}

	/**
	 * @param sSN the sSN to set
	 */
	public void setSSN(String sSN) {
		SSN = sSN;
	}

	/**
	 * @return the i94
	 */
	public String getI94() {
		return i94;
	}

	/**
	 * @param i94 the i94 to set
	 */
	public void setI94(String i94) {
		this.i94 = i94;
	}

	/**
	 * @return the passport_number
	 */
	public String getPassport_number() {
		return passport_number;
	}

	/**
	 * @param passport_number the passport_number to set
	 */
	public void setPassport_number(String passport_number) {
		this.passport_number = passport_number;
	}

	/**
	 * @return the passport_issued_country
	 */
	public String getPassport_issued_country() {
		return passport_issued_country;
	}

	/**
	 * @param passport_issued_country the passport_issued_country to set
	 */
	public void setPassport_issued_country(String passport_issued_country) {
		this.passport_issued_country = passport_issued_country;
	}

	/**
	 * @return the passport_exp_date
	 */
	public String getPassport_exp_date() {
		return passport_exp_date;
	}

	/**
	 * @param passport_exp_date the passport_exp_date to set
	 */
	public void setPassport_exp_date(String passport_exp_date) {
		this.passport_exp_date = passport_exp_date;
	}

	/**
	 * @return the sevis
	 */
	public String getSevis() {
		return sevis;
	}

	/**
	 * @param sevis the sevis to set
	 */
	public void setSevis(String sevis) {
		this.sevis = sevis;
	}

	/**
	 * @return the last_arrival_date
	 */
	public String getLast_arrival_date() {
		return last_arrival_date;
	}

	/**
	 * @param last_arrival_date the last_arrival_date to set
	 */
	public void setLast_arrival_date(String last_arrival_date) {
		this.last_arrival_date = last_arrival_date;
	}

	/**
	 * @return the last_arrival_place
	 */
	public String getLast_arrival_place() {
		return last_arrival_place;
	}

	/**
	 * @param last_arrival_place the last_arrival_place to set
	 */
	public void setLast_arrival_place(String last_arrival_place) {
		this.last_arrival_place = last_arrival_place;
	}

	/**
	 * @return the immigration_status
	 */
	public String getImmigration_status() {
		return immigration_status;
	}

	/**
	 * @param immigration_status the immigration_status to set
	 */
	public void setImmigration_status(String immigration_status) {
		this.immigration_status = immigration_status;
	}

	/**
	 * @return the curr_immigration_status
	 */
	public String getCurr_immigration_status() {
		return curr_immigration_status;
	}

	/**
	 * @param curr_immigration_status the curr_immigration_status to set
	 */
	public void setCurr_immigration_status(String curr_immigration_status) {
		this.curr_immigration_status = curr_immigration_status;
	}

	/**
	 * @return the fname
	 */
	public String getFname() {
		return fname;
	}

	/**
	 * @param fname the fname to set
	 */
	public void setFname(String fname) {
		this.fname = fname;
	}

	/**
	 * @return the lname
	 */
	public String getLname() {
		return lname;
	}

	/**
	 * @param lname the lname to set
	 */
	public void setLname(String lname) {
		this.lname = lname;
	}

	/**
	 * @return the mname
	 */
	public String getMname() {
		return mname;
	}

	/**
	 * @param mname the mname to set
	 */
	public void setMname(String mname) {
		this.mname = mname;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the marital
	 */
	public String getMarital() {
		return marital;
	}

	/**
	 * @param marital the marital to set
	 */
	public void setMarital(String marital) {
		this.marital = marital;
	}

	/**
	 * @return the mailAdd_name
	 */
	public String getMailAdd_name() {
		return mailAdd_name;
	}

	/**
	 * @param mailAdd_name the mailAdd_name to set
	 */
	public void setMailAdd_name(String mailAdd_name) {
		this.mailAdd_name = mailAdd_name;
	}

	/**
	 * @return the mailAdd_st
	 */
	public String getMailAdd_st() {
		return mailAdd_st;
	}

	/**
	 * @param mailAdd_st the mailAdd_st to set
	 */
	public void setMailAdd_st(String mailAdd_st) {
		this.mailAdd_st = mailAdd_st;
	}

	/**
	 * @return the mailAdd_type
	 */
	public String getMailAdd_type() {
		return mailAdd_type;
	}

	/**
	 * @param mailAdd_type the mailAdd_type to set
	 */
	public void setMailAdd_type(String mailAdd_type) {
		this.mailAdd_type = mailAdd_type;
	}

	/**
	 * @return the mailAdd_number
	 */
	public String getMailAdd_number() {
		return mailAdd_number;
	}

	/**
	 * @param mailAdd_number the mailAdd_number to set
	 */
	public void setMailAdd_number(String mailAdd_number) {
		this.mailAdd_number = mailAdd_number;
	}

	/**
	 * @return the mailAdd_city
	 */
	public String getMailAdd_city() {
		return mailAdd_city;
	}

	/**
	 * @param mailAdd_city the mailAdd_city to set
	 */
	public void setMailAdd_city(String mailAdd_city) {
		this.mailAdd_city = mailAdd_city;
	}

	/**
	 * @return the mailAdd_state
	 */
	public String getMailAdd_state() {
		return mailAdd_state;
	}

	/**
	 * @param mailAdd_state the mailAdd_state to set
	 */
	public void setMailAdd_state(String mailAdd_state) {
		this.mailAdd_state = mailAdd_state;
	}

	/**
	 * @return the mailAdd_zip
	 */
	public String getMailAdd_zip() {
		return mailAdd_zip;
	}

	/**
	 * @param mailAdd_zip the mailAdd_zip to set
	 */
	public void setMailAdd_zip(String mailAdd_zip) {
		this.mailAdd_zip = mailAdd_zip;
	}

	/**
	 * @return the country1
	 */
	public String getCountry1() {
		return country1;
	}

	/**
	 * @param country1 the country1 to set
	 */
	public void setCountry1(String country1) {
		this.country1 = country1;
	}

	/**
	 * @return the country2
	 */
	public String getCountry2() {
		return country2;
	}

	/**
	 * @param country2 the country2 to set
	 */
	public void setCountry2(String country2) {
		this.country2 = country2;
	}

	/**
	 * @return the birth_city
	 */
	public String getBirth_city() {
		return birth_city;
	}

	/**
	 * @param birth_city the birth_city to set
	 */
	public void setBirth_city(String birth_city) {
		this.birth_city = birth_city;
	}

	/**
	 * @return the birth_state
	 */
	public String getBirth_state() {
		return birth_state;
	}

	/**
	 * @param birth_state the birth_state to set
	 */
	public void setBirth_state(String birth_state) {
		this.birth_state = birth_state;
	}

	/**
	 * @return the birth_country
	 */
	public String getBirth_country() {
		return birth_country;
	}

	/**
	 * @param birth_country the birth_country to set
	 */
	public void setBirth_country(String birth_country) {
		this.birth_country = birth_country;
	}

	/**
	 * @return the birth_dob
	 */
	public String getBirth_dob() {
		return birth_dob;
	}

	/**
	 * @param birth_dob the birth_dob to set
	 */
	public void setBirth_dob(String birth_dob) {
		this.birth_dob = birth_dob;
	}

	/**
	 * @return the physicalAddressConfirmation
	 */
	public String getPhysicalAddressConfirmation() {
		return physicalAddressConfirmation;
	}

	/**
	 * @param physicalAddressConfirmation the physicalAddressConfirmation to set
	 */
	public void setPhysicalAddressConfirmation(String physicalAddressConfirmation) {
		this.physicalAddressConfirmation = physicalAddressConfirmation;
	}

}
