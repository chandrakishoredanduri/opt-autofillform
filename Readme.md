Hello welcome to my Project,
In this project we achieve the goal of automatically filling the application forms using java and spring framework.

## Prerequisets :##
Apache Tomcat Server and Eclipse IDE or any of your favourite IDE.

# To Run this project

1. Clone the repo into your local computer.
2. Install Apache Tomcat Server.
3. Import project into your workspace.
4. Click run
5. Open browser and type https://localhost:8080/opt-autofillform/
6. select initial opt from the drop-down-menu.
7. Start filling the details and click on submit.
8. Download the PDF.
